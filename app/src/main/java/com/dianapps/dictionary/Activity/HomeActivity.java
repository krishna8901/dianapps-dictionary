package com.dianapps.dictionary.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.dianapps.dictionary.ApiEndPoint;
import com.dianapps.dictionary.Model.DailyWordResponseModel;
import com.dianapps.dictionary.Model.WordListResponseModel;
import com.dianapps.dictionary.R;
import com.dianapps.dictionary.SearchWord;
import com.dianapps.dictionary.ServiceGenerator;
import com.dianapps.dictionary.WordListAdapter;
import com.dianapps.dictionary.databinding.ActivityHomeBinding;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.orhanobut.hawk.Hawk;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {

    public DrawerLayout drawerLayout;
    public ActionBarDrawerToggle actionBarDrawerToggle;
    ActivityHomeBinding binding;
    ApiEndPoint apiEndPoint;
    Call<DailyWordResponseModel> call;
    Call<List<WordListResponseModel>> wordlistCall;
    List<String> listNum = new ArrayList<>();
    GoogleSignInOptions gso;
    GoogleSignInClient mGoogleSignInClient;

    TextToSpeech tts;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);

        DailyWord();
        RandomWord();
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if (account != null) {
            String personName = account.getDisplayName();
            binding.tool.adminName.setText(personName);
            View headerView = binding.navigate.getHeaderView(0);
            TextView navUsername =headerView.findViewById(R.id.admin);
            navUsername.setText(personName);
        }

        binding.tool.menu.setOnClickListener(view -> {
            if (binding.myDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            } else {
                binding.myDrawerLayout.openDrawer(GravityCompat.START);
            }
        });

        binding.navigate.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.logout:
                        Hawk.delete("token");
                        mGoogleSignInClient.signOut().addOnCompleteListener(HomeActivity.this, new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                                startActivity(intent);
                                finish();

                            }
                        });
                        break;

                    case R.id.share:
                        Intent shareIntent= new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        String shareBody="Download DianApps Application:-https://play.google.com/store/apps/details?id=com.mobisystems.msdict.embedded.wireless.oxford.dictionaryofenglish&hl=en_US&gl=US";
                        String sharesub="DianApps Dictionary";
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT,sharesub);
                        shareIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                        startActivity(Intent.createChooser(shareIntent, "Share Via"));
                        break;

                    case R.id.rate:
                        Uri uri=Uri.parse("https://play.google.com/store/apps/details?id=com.mobisystems.msdict.embedded.wireless.oxford.dictionaryofenglish&hl=en_US&gl=US"+getApplicationContext());
                        Intent i  =new Intent(Intent.ACTION_VIEW,uri);
                        try {
                            startActivity(i);
                        } catch (Exception e) {
                            Toast.makeText(HomeActivity.this, "Unable to open\n"+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case R.id.save:
                        Intent in = new Intent(HomeActivity.this, SaveWordActivity.class);
                        startActivity(in);
                        break;

                    case R.id.word:
                        Intent inn= new Intent(HomeActivity.this,SaveWordActivity.class);
                        startActivity(inn);
                        break;

                    case R.id.history:
                        Intent intt= new Intent(HomeActivity.this,SaveWordActivity.class);
                        startActivity(intt);
                        break;

                    case R.id.version:
                        Toast.makeText(HomeActivity.this, "You have a latest version", Toast.LENGTH_SHORT).show();
                        break;
                }
                return false;
            }
        });

//        @Override
//        public boolean onOptionsItemSelected(@NonNull MenuItem item) {
//            switch (item.getItemId()){
//                case R.id.logout:
//                    Toast.makeText(this, "hello", Toast.LENGTH_SHORT).show();
//                    Hawk.delete("token");
//                    Intent intent = new Intent(this,LoginActivity.class);
//                    startActivity(intent);
//                    finish();
//                    return true;
//
//            }
//            return super.onOptionsItemSelected(item);
//        }

        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if (i != TextToSpeech.ERROR) {
                    tts.setLanguage(Locale.ENGLISH);
                }
            }
        });

        binding.word.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.object.setVisibility(View.VISIBLE);
                binding.date.setVisibility(View.VISIBLE);
                binding.volume.setVisibility(View.VISIBLE);
                binding.save.setVisibility(View.VISIBLE);
                binding.share.setVisibility(View.VISIBLE);
            }
        });
        binding.tool.search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, SearchWord.class);
                startActivity(intent);
            }
        });

    }
    public void sharetextOnly(String title) {
        String sharebody = title;
        Intent intent = new Intent(Intent.ACTION_SEND);

        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Subject Here");

        intent.putExtra(Intent.EXTRA_TEXT, sharebody);
        startActivity(Intent.createChooser(intent, "Share Via"));
    }

//        actionBarDrawerToggle = new ActionBarDrawerToggle(this, binding.myDrawerLayout, R.string.nav_open, R.string.nav_close);
//
//        binding.myDrawerLayout.addDrawerListener(actionBarDrawerToggle);
//        actionBarDrawerToggle.syncState();

    // to make the Navigation drawer icon always appear on the action bar
    //   getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    //    @Override
//    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
//
//        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void DailyWord() {
        Hawk.init(this).build();
        apiEndPoint = ServiceGenerator.createService(ApiEndPoint.class);
//        String accessToken = Hawk.get("deviceToken");
        String accessToken = "q095std75p9uysknwxmaxeyw0zqzsaa0spj8jln0rm9tiv653";
        String date = "20220-02-24";

//        Calendar calendar = Calendar.getInstance();
//        Date today = calendar.getTime();

        call = apiEndPoint.getDailyWord(date, accessToken);
        call.enqueue(new Callback<DailyWordResponseModel>() {
            @Override
            public void onResponse(Call<DailyWordResponseModel> call, Response<DailyWordResponseModel> response) {
                if (response.isSuccessful()) {
                    Hawk.put("token", accessToken);
                    binding.word.setText(response.body().getWord());
                    binding.date.setText(response.body().getPdd());
                    binding.object.setText("Note-  " + response.body().getNote());

                    binding.volume.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            tts.speak(response.body().getWord(), TextToSpeech.QUEUE_FLUSH, null);
                        }
                    });

                    binding.share.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            sharetextOnly(response.body().getWord().toString());
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<DailyWordResponseModel> call, Throwable t) {

            }
        });
    }

    public void RandomWord() {
        apiEndPoint = ServiceGenerator.createService(ApiEndPoint.class);
        String accessToken = "q095std75p9uysknwxmaxeyw0zqzsaa0spj8jln0rm9tiv653";
        String word = "xyz";
        wordlistCall = apiEndPoint.getRandomWord(word, accessToken);
        wordlistCall.enqueue(new Callback<List<WordListResponseModel>>() {
            @Override
            public void onResponse(Call<List<WordListResponseModel>> call, Response<List<WordListResponseModel>> response) {
                if (response.isSuccessful()) {

                    binding.wordListRecyclerview.setLayoutManager(new LinearLayoutManager(getApplication()));
                    List<String> wordlist = new ArrayList<>();
                    for (int i = 0; i < response.body().size(); i++)
                        wordlist.add(response.body().get(i).getWord());
                    binding.wordListRecyclerview.setAdapter(new WordListAdapter(wordlist));

                }
            }

            @Override
            public void onFailure(Call<List<WordListResponseModel>> call, Throwable t) {
            }
        });
    }
}
