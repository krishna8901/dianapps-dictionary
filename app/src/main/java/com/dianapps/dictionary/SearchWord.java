package com.dianapps.dictionary;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.dianapps.dictionary.Model.SearchListResponseModel;
import com.dianapps.dictionary.databinding.ActivitySearchWordBinding;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchWord extends AppCompatActivity {
    ApiEndPoint apiEndPoint;
    Call<List<SearchListResponseModel>> call;
    List<String> listNum = new ArrayList<>();
    ActivitySearchWordBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_search_word);
        binding.back.setOnClickListener(view -> onBackPressed());

        binding.search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(SearchWord.this, "API Hited", Toast.LENGTH_SHORT).show();
                SearchWordApi();
            }
        });

//        binding.etSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                ServiceGenerator serviceGenerator= new ServiceGenerator();
//                return true;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                return false;
//            }
//        });

        }
    public void SearchWordApi(){
        apiEndPoint = ServiceGenerator.createService(ApiEndPoint.class);
        String accessToken = "q095std75p9uysknwxmaxeyw0zqzsaa0spj8jln0rm9tiv653";
        String date = "20220-02-22";

        call = apiEndPoint.getRandomSearchWord(date,accessToken);
        call.enqueue(new Callback<List<SearchListResponseModel>>() {
            @Override
            public void onResponse(Call<List<SearchListResponseModel>> call, Response<List<SearchListResponseModel>> response) {
                if(response.isSuccessful()){
                    binding.rvSearch.setLayoutManager(new LinearLayoutManager(getApplication()));
                    List<String> word = new ArrayList<>();
                    for (int i = 0; i < response.body().size(); i++)
                        word.add(response.body().get(i).getWord());
                    binding.rvSearch.setAdapter(new WordListAdapter(word));
                }
            }
            @Override
            public void onFailure(Call<List<SearchListResponseModel>> call, Throwable t) {

            }
        });
    }
}