package com.dianapps.dictionary;

import com.dianapps.dictionary.Model.DailyWordResponseModel;
import com.dianapps.dictionary.Model.SearchListResponseModel;
import com.dianapps.dictionary.Model.WordListResponseModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiEndPoint {

    @GET("v4/words.json/wordOfTheDay")
    Call<DailyWordResponseModel> getDailyWord(@Query("date") String date, @Query("api_key") String api_key);

    @GET("v4/words.json/randomWords")
    Call<List<WordListResponseModel>> getRandomWord(@Query("word") String word, @Query("api_key") String api_key);

    @GET("v4/words.json/randomWords")
    Call<List<SearchListResponseModel>> getRandomSearchWord(@Query("word") String word, @Query("api_key") String api_key);

}