package com.dianapps.dictionary;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.dianapps.dictionary.Activity.HomeActivity;
import com.dianapps.dictionary.Activity.LoginActivity;
import com.orhanobut.hawk.Hawk;

@SuppressLint("CustomSplashScreen")
public class SplashScreen extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash_layout);
        Hawk.init(this).build();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String token = Hawk.get("token",null);
                Intent intent;
                if(token == null) {
                    intent = new Intent(SplashScreen.this, LoginActivity.class);
                }else{
                    intent = new Intent(SplashScreen.this, HomeActivity.class);
                }
                startActivity(intent);
                finish();
            }
        },2000);
    }
}