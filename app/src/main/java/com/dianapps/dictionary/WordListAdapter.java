package com.dianapps.dictionary;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.dianapps.dictionary.databinding.WordListLayoutBinding;
import com.orhanobut.hawk.Hawk;

import java.util.List;
import java.util.Random;

public class WordListAdapter extends RecyclerView.Adapter<WordListAdapter.VHolder> {

    List<String> list;
    public WordListAdapter(List<String> list){
        this.list=list;
    }
   // String lettar;

    @NonNull
    @Override
    public VHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        WordListLayoutBinding binding= DataBindingUtil.inflate(inflater,R.layout.word_list_layout,parent,false);
        return new WordListAdapter.VHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull VHolder holder, int position) {
        WordListLayoutBinding binding=(WordListLayoutBinding) holder.getBinding();
        binding.layoutText.setText(list.get(position));
//      binding.tvFirst.setText(list.get(position).substring(0,1));
//        Random mRandom = new Random();
//        int color = Color.argb(215, mRandom.nextInt(216), mRandom.nextInt(216), mRandom.nextInt(256));
//        ((GradientDrawable) binding.tvFirst.getBackground()).setColor(color);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class VHolder extends RecyclerView.ViewHolder {
        WordListLayoutBinding binding;
        public VHolder(@NonNull WordListLayoutBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }
        public WordListLayoutBinding getBinding(){
            return binding;
        }
    }
}
